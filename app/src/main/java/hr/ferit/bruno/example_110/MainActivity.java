package hr.ferit.bruno.example_110;

import android.app.Activity;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends Activity {

    private static final String SENSOR_TAG = "Sensor";

    private SensorManager mSensorManager;
    private Sensor mAccelerationSensor;
    private SensorEventListener mAccelerationListener;

    @BindView(R.id.rlRoot) RelativeLayout rlRoot;
    @BindView(R.id.tvAcceleration) TextView tvAcceleration;
    @BindView(R.id.tvAccPerComponent) TextView tvAccPerComponent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        // Display all sensor info:
        this.mSensorManager = (SensorManager) this.getSystemService(SENSOR_SERVICE);
        List<Sensor> allSensors = this.mSensorManager.getSensorList(Sensor.TYPE_ALL);
        for (Sensor sensor : allSensors){
            Log.d(SENSOR_TAG, sensor.getName() + ": " + sensor.toString());
        }

        this.mAccelerationSensor =
                this.mSensorManager.getDefaultSensor (Sensor.TYPE_ACCELEROMETER);

        this.mAccelerationListener = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent event) {
                if(event.sensor.getType() == Sensor.TYPE_ACCELEROMETER){
                    updateUiWithSensorInfo(event);
                }
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) { }
        };
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(this.mAccelerationSensor != null && this.mAccelerationListener != null){
            this.mSensorManager.registerListener(this.mAccelerationListener,
                    this.mAccelerationSensor, SensorManager.SENSOR_DELAY_NORMAL);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(this.mAccelerationSensor != null && this.mAccelerationListener != null) {
            this.mSensorManager.unregisterListener(this.mAccelerationListener);
        }
    }

    private void updateUiWithSensorInfo(SensorEvent event) {
        double xAcc = event.values[0];
        double yAcc = event.values[1];
        double zAcc = event.values[2];
        double totalAcc = Math.sqrt(xAcc*xAcc + yAcc*yAcc + zAcc*zAcc);
        DecimalFormat decimalFormat = new DecimalFormat("0.00");
        String msgTotal = decimalFormat.format(totalAcc);
        String msgComponent = xAcc + "\n" + yAcc + "\n" + zAcc;
        int color = totalAcc > 12 ? Color.parseColor("#cd4f39") : Color.parseColor("#3cb371");
        tvAcceleration.setText(msgTotal);
        tvAccPerComponent.setText(msgComponent);
        rlRoot.setBackgroundColor(color);
    }
}